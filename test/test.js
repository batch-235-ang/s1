const { assert } = require("chai");
const { newUser } = require("../index.js").newUser;
const { user } = require("../index.js").user;

describe("Test newUser object", () => {
	it("Assert newUser type is an object", () => {
		assert.equal(typeof(newUser), "object");
	})

	it("Assert newUser email is type string", () => {
		assert.equal(typeof(newUser.email), "string");
	})

	it("Assert newUser email is not undefined", () => {
		assert.notEqual(typeof(newUser.email), "undefined");
	})

	it("Assert newUser password is type string", () => {
		assert.equal(typeof(newUser.password), "string");
	})

	it("Asser newUser password is at least 16 characters long", () => {
		assert.isAtLeast(newUser.password.length, 16);
	})
})

describe("Test user object", () => {
	it("Assert user firstName is type string", () => {
		assert.equal(typeof(user.firstName), "string");
	})
	it("Assert user lastName is type string", () => {
		assert.equal(typeof(user.lastName), "string");
	})
	it("Assert user firstName is not undefined", () => {
		assert.notEqual(typeof(user.firstName), "undefined");
	})
	it("Assert user lastName is not undefined", () => {
		assert.notEqual(typeof(user.lastName), "undefined");
	})
	it("Assert user age is at least 18", () => {
		assert.isAtLeast(user.age, 18);
	})
	it("Assert user age is type number", () => {
		assert.equal(typeof(user.age), "number");
	})
	it("Assert user contact number is type string", () => {
		assert.equal(typeof(user.contact), "string");
	})
	it("Assert user batch number is type number", () => {
		assert.equal(typeof(user.batchNumber), "number");
	})
	it("Assert user batch number is not undefined", () => {
		assert.notEqual(user.batch, "undefined");
	})
	it("Assert user password is at least 16 characters", () => {
		assert.isAtLeast(user.password.length, 16);
	})
})